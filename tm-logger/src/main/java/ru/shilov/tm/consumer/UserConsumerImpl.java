package ru.shilov.tm.consumer;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.IConsumer;

import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import java.nio.file.Path;
import java.nio.file.Paths;

@AllArgsConstructor
public final class UserConsumerImpl implements IConsumer {

    @NotNull
    private final Session session;

    @Override
    @SneakyThrows
    public void init() {
        @NotNull final Destination destination = session.createTopic("User");
        @NotNull final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(this);
    }

    @NotNull
    @Override
    public Path getPath() {
        return Paths.get(System.getProperty("user.dir") + "/log/" + "user.log");
    }

}
