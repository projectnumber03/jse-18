package ru.shilov.tm.api;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;

public interface IConsumer extends MessageListener {

    @Override
    @SneakyThrows
    default void onMessage(@NotNull final Message message) {
        if (!(message instanceof TextMessage)) return;
        save(((TextMessage) message).getText(), getPath());
    }

    default void save(@NotNull final String message, @Nullable final Path path) throws IOException {
        if (path == null) return;
        try (@NotNull final FileWriter fileWriter = new FileWriter(path.toFile(), true)) {
            fileWriter.write(message);
            fileWriter.flush();
        }
    }

    void init();

    @NotNull
    Path getPath();

}
