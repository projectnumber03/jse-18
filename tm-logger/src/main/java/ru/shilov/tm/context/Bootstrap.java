package ru.shilov.tm.context;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.consumer.ProjectConsumerImpl;
import ru.shilov.tm.consumer.SessionConsumerImpl;
import ru.shilov.tm.consumer.TaskConsumerImpl;
import ru.shilov.tm.consumer.UserConsumerImpl;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Session;
import java.io.File;

public final class Bootstrap {

    @SneakyThrows
    public void init() {
        new File(System.getProperty("user.dir") + "/log").mkdirs();
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(System.getProperty("broker.url"));
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        new ProjectConsumerImpl(session).init();
        new TaskConsumerImpl(session).init();
        new UserConsumerImpl(session).init();
        new SessionConsumerImpl(session).init();
    }

}
