package ru.shilov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.ISystemEndPoint;
import ru.shilov.tm.context.Bootstrap;
import ru.shilov.tm.entity.User;

import javax.jws.WebService;
import java.util.Arrays;

@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.shilov.tm.api.endpoint.ISystemEndPoint")
public final class SystemEndPointImpl implements ISystemEndPoint {

    @NotNull
    private Bootstrap bootstrap;

    @NotNull
    @Override
    public String getHost(@Nullable final String token) throws Exception {
        bootstrap.getSessionService().validate(token, Arrays.asList(User.Role.ADMIN));
        return bootstrap.getSettingService().getProperty("hostname");
    }

    @NotNull
    @Override
    public String getPort(@Nullable final String token) throws Exception {
        bootstrap.getSessionService().validate(token, Arrays.asList(User.Role.ADMIN));
        return System.getProperty("server.port");
    }

}
