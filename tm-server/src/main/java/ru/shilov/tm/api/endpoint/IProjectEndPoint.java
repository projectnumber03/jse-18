package ru.shilov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndPoint {

    @WebMethod
    @NotNull
    List<ProjectDTO> findAllProjects(@Nullable final String token) throws Exception;

    @WebMethod
    @NotNull
    List<ProjectDTO> findAllProjectsOrderByFinish(@Nullable final String token) throws Exception;

    @WebMethod
    @NotNull
    List<ProjectDTO> findAllProjectsOrderByStart(@Nullable final String token) throws Exception;

    @WebMethod
    @NotNull
    List<ProjectDTO> findAllProjectsOrderByStatus(@Nullable final String token) throws Exception;

    @WebMethod
    @NotNull
    ProjectDTO findOneProject(@Nullable final String token, @NotNull final String id) throws Exception;

    @WebMethod
    @NotNull
    List<ProjectDTO> findProjectsByUserId(@Nullable final String token) throws Exception;

    @WebMethod
    void removeAllProjects(@Nullable final String token) throws Exception;

    @WebMethod
    void removeProjectsByUserId(@Nullable final String token) throws Exception;

    @WebMethod
    void removeOneProjectByUserId(@Nullable final String token, @Nullable final String id) throws Exception;

    @WebMethod
    @Nullable
    ProjectDTO persistProject(@Nullable final String token, @NotNull final ProjectDTO p) throws Exception;

    @Nullable
    @WebMethod
    ProjectDTO mergeProject(@Nullable final String token, @NotNull final ProjectDTO p) throws Exception;

    @NotNull
    List<ProjectDTO> findProjectsByNameOrDescription(@Nullable final String token, @NotNull final String value) throws Exception;

}
