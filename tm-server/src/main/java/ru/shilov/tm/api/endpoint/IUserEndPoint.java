package ru.shilov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.dto.UserDTO;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService
public interface IUserEndPoint {

    @NotNull
    @WebMethod
    List<UserDTO> findAllUsers(@Nullable final String token) throws Exception;

    @NotNull
    @WebMethod
    UserDTO findOneUser(@Nullable final String token) throws Exception;@NotNull

    @WebMethod
    UserDTO findOneUserById(@Nullable final String token, @Nullable final String userId) throws Exception;

    @WebMethod
    void removeAllUsers(@Nullable final String token) throws Exception;

    @Nullable
    @WebMethod
    UserDTO persistUser(@Nullable final String token, @Nullable final UserDTO u) throws Exception;

    @Nullable
    @WebMethod
    UserDTO mergeUser(@Nullable final String token, @NotNull final UserDTO u) throws Exception;

    @WebMethod
    void removeOneUserById(@Nullable final String token, @Nullable final String id) throws Exception;

    @Nullable
    @WebMethod
    UserDTO registerUser(@Nullable final UserDTO u) throws Exception;

}
