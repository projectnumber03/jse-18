package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IRepository;

import javax.persistence.EntityManager;
import java.util.List;

public interface IService<T> {

    @NotNull
    List<T> findAll();

    @NotNull
    T findOne(@Nullable final String id) throws Exception;

    void removeAll();

    @Nullable
    T persist(@Nullable final T entity) throws Exception;

    @Nullable
    T merge(@Nullable final T entity) throws Exception;

    IRepository<T> getRepository(@NotNull final EntityManager entityManager);

}
