package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.error.NumberToIdTransformException;

public interface IUserService extends IService<User> {

    @NotNull
    User findByLogin(@Nullable final String login) throws NoSuchEntityException;

    void removeOneById(@Nullable final String id) throws Exception;

    @Nullable
    User register(@Nullable final User user);

}
