import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.shilov.tm.api.endpoint.*;
import ru.shilov.tm.context.Bootstrap;

import java.lang.Exception;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.Assert.*;

public class ProjectOperationTest {

    class ProjectFactory {

        private int count = 1;

        private ProjectDTO createProject() {
            @NotNull final ProjectDTO p = new ProjectDTO();
            p.setName("project" + count);
            p.setDescription("description" + count);
            p.setStart(DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now()));
            p.setFinish(DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(1)));
            p.setStatus(Status.PLANNED);
            count++;
            return p;
        }

        private void createProjects(@NotNull final Bootstrap b, final int count) throws Exception {
            for (int i = 0; i < count; i++) {
                b.getProjectEndPoint().persistProject(b.getToken(), createProject());
            }
        }

    }

    @Before
    public void registerUser() throws Exception {
        @NotNull final Bootstrap b = new Bootstrap();
        @NotNull final UserDTO testUser = new UserDTO();
        testUser.setLogin("test");
        testUser.setPassword("test");
        testUser.setRole(Role.ADMIN);
        b.getUserEndPoint().registerUser(testUser);
    }

    @After
    public void deleteUser() throws Exception {
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        @Nullable final String idToDelete = b.getUserEndPoint().findAllUsers(b.getToken()).stream()
                .filter(u -> "test".equals(u.getLogin()))
                .map(UserDTO::getId).findAny().orElse(null);
        b.getUserEndPoint().removeOneUserById(b.getToken(), idToDelete);
    }

    @Test
    public void clearTest() throws Exception {
        @NotNull final ProjectFactory pf = new ProjectFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
        pf.createProjects(b, 10);
        @NotNull List<ProjectDTO> projects = b.getProjectEndPoint().findProjectsByUserId(b.getToken());
        assertEquals(10, projects.size());
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
        projects = b.getProjectEndPoint().findProjectsByUserId(b.getToken());
        assertEquals(0, projects.size());
    }

    @Test
    public void findAllByUserIdTest() throws Exception {
        @NotNull final ProjectFactory pf = new ProjectFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
        pf.createProjects(b, 10);
        @NotNull List<ProjectDTO> projects = b.getProjectEndPoint().findProjectsByUserId(b.getToken());
        assertEquals(10, projects.size());
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
    }

    @Test
    public void findByNameOrDescriptionTest() throws Exception {
        @NotNull final ProjectFactory pf = new ProjectFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
        pf.createProjects(b, 10);
        @NotNull List<ProjectDTO> projects = b.getProjectEndPoint().findProjectsByNameOrDescription(b.getToken(), "project1");
        assertEquals(2, projects.size());
        projects = b.getProjectEndPoint().findProjectsByNameOrDescription(b.getToken(), "description1");
        assertEquals(2, projects.size());
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
    }

    @Test
    public void findOneTest() throws Exception {
        @NotNull final ProjectFactory pf = new ProjectFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
        pf.createProjects(b, 10);
        @Nullable final ProjectDTO projectToFind = b.getProjectEndPoint().findProjectsByNameOrDescription(b.getToken(), "project2").stream().findAny().orElse(null);
        assertNotNull(projectToFind);
        @NotNull final ProjectDTO project = b.getProjectEndPoint().findOneProject(b.getToken(), projectToFind.getId());
        assertEquals(project.getId(), projectToFind.getId());
        assertEquals(project.getName(), projectToFind.getName());
        assertEquals(project.getDescription(), projectToFind.getDescription());
        assertEquals(project.getStart(), projectToFind.getStart());
        assertEquals(project.getFinish(), projectToFind.getFinish());
        assertEquals(project.getUserId(), projectToFind.getUserId());
        assertEquals(project.getStatus(), projectToFind.getStatus());
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
    }

    @Test
    public void mergeTest() throws Exception {
        @NotNull final ProjectFactory pf = new ProjectFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
        pf.createProjects(b, 10);
        @Nullable final ProjectDTO projectToMerge = b.getProjectEndPoint().findProjectsByNameOrDescription(b.getToken(), "project1").stream().findAny().orElse(null);
        assertNotNull(projectToMerge);
        projectToMerge.setName("project-1");
        projectToMerge.setDescription("description-1");
        projectToMerge.setStart(DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(2)));
        projectToMerge.setFinish(DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(3)));
        projectToMerge.setStatus(Status.IN_PROCESS);
        b.getProjectEndPoint().mergeProject(b.getToken(), projectToMerge);
        @NotNull final List<ProjectDTO> empty = b.getProjectEndPoint().findProjectsByNameOrDescription(b.getToken(), "test-project1");
        assertEquals(0, empty.size());
        @Nullable final ProjectDTO project = b.getProjectEndPoint().findProjectsByNameOrDescription(b.getToken(), "project-1").stream().findAny().orElse(null);
        assertNotNull(project);
        assertEquals(project.getName(), "project-1");
        assertEquals(project.getDescription(), "description-1");
        assertEquals(project.getStart(), DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(2)));
        assertEquals(project.getFinish(), DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(3)));
        assertEquals(project.getStatus(), Status.IN_PROCESS);
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
    }

    @Test
    public void persistTest() throws Exception {
        @NotNull final ProjectFactory pf = new ProjectFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
        b.getProjectEndPoint().persistProject(b.getToken(), pf.createProject());
        @Nullable final ProjectDTO project = b.getProjectEndPoint().findProjectsByNameOrDescription(b.getToken(), "project1").stream().findAny().orElse(null);
        assertNotNull(project);
        assertEquals(project.getName(), "project1");
        assertEquals(project.getDescription(), "description1");
        assertEquals(project.getStart(), DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now()));
        assertEquals(project.getFinish(), DateTimeFormatter.ISO_LOCAL_DATE.format(LocalDate.now().plusDays(1)));
        assertEquals(project.getStatus(), Status.PLANNED);
        @NotNull final List<ProjectDTO> projects = b.getProjectEndPoint().findProjectsByUserId(b.getToken());
        assertEquals(1, projects.size());
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
    }

    @Test
    public void removeTest() throws Exception {
        @NotNull final ProjectFactory pf = new ProjectFactory();
        @NotNull final Bootstrap b = new Bootstrap();
        b.setToken(b.getSessionEndPoint().createSession("test", "test"));
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
        pf.createProjects(b, 10);
        @Nullable final ProjectDTO projectToDelete = b.getProjectEndPoint().findProjectsByNameOrDescription(b.getToken(), "project2").stream().findAny().orElse(null);
        assertNotNull(projectToDelete);
        b.getProjectEndPoint().removeOneProjectByUserId(b.getToken(), projectToDelete.getId());
        @NotNull final List<ProjectDTO> projects = b.getProjectEndPoint().findProjectsByUserId(b.getToken());
        assertEquals(9, projects.size());
        assertFalse(projects.stream().map(ProjectDTO::getName).anyMatch("project2"::equals));
        b.getProjectEndPoint().removeProjectsByUserId(b.getToken());
    }

}
