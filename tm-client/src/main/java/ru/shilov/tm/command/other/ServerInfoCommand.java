package ru.shilov.tm.command.other;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class ServerInfoCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String token = getToken();
        System.out.println("HOST: " + getEndPointLocator().getSystemEndPoint().getHost(token));
        System.out.println("PORT: " + getEndPointLocator().getSystemEndPoint().getPort(token));
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "server-info";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Вывод информации о ноде";
    }

}
