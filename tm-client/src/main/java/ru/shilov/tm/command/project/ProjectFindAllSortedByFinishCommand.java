package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.ProjectDTO;
import ru.shilov.tm.command.AbstractTerminalCommand;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public final class ProjectFindAllSortedByFinishCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final List<ProjectDTO> projects = getEndPointLocator().getProjectEndPoint().findAllProjectsOrderByFinish(getToken());
        getServiceLocator().getTerminalService().printAllProjects(projects);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list-finish";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Список проектов по дате окончания";
    }

}
